from odoo import models, fields, api
# for checking the date validity:
from odoo.exceptions import ValidationError

class LibraryBook(models.Model):

	_name = 'library.book'
	_description = 'A book in a../lms-evaluation/odoo/addons/my_library library.'
	_order = 'date_released desc, name'
	_rec_name = 'name' # this is default

	# check that name is unique
	# using sql constraint
	_sql_constraints = [
		('name_uniq',
		'UNIQUE (name)',
		'Book title must be unique.')
		]

	name = fields.Char('Book Title', required = True, index = True)
	date_released = fields.Date('Release date')

	# check that date_released is not in future:
	@api.constrains('date_released')
	def _check_release_date(self):
		for record in self:
			if record.date_released and record.date_released > fields.Date.today():
				raise models.ValidationError('Release date must be in the past')

	date_updated = fields.Datetime('Last updated')
	author_id = fields.Many2many('res.partner', string = 'Authors')
	pages = fields.Integer('Number of pages')
	notes = fields.Text('Internal notes')
	state = fields.Selection(
		[
		('draft', 'Not Available'),
		('available', 'Available'),
		('lost', 'Lost'),
		],
		'State', default = 'draft')
	description = fields.Html('Description', sanitize = True, strip_style = False)
	cover = fields.Binary('Book Cover', attachment=True)
	out_of_print = fields.Boolean('Out of print?')
	reader_rating = fields.Float(
		'Reader\'s Average Rating',
		digits = (14, 4) # Optional precision (total, decimals)
		)
	# for the publisher_id m2o field, we have to modify the res.partner model too, add a corresponding o2m field
	publisher_id = fields.Many2one(
		'res.partner', string = 'Publisher',
		ondelete = 'set null',
		context = {},
		domain = [],
		)
	# category_id is not visible in the UI yet
	category_id = fields.Many2one('library.book.category')
	# problems ("issues") with the book
	book_issue_id = fields.One2many('book.issue', 'book_id')

# this is the reverse one2many relation to the publisher_id many2one in the library.book model
class ResPartner(models.Model):

	_inherit = 'res.partner'

	published_books_ids = fields.One2many(
		'library.book', 'publisher_id',
		string = 'Published books')
	authored_books_ids = fields.Many2many(
		'library.book', 
		string = 'Authored books',
		# relation='library_book_res_partner_rel'  # optional
		)
	count_books = fields.Integer('Number of authored books', compute = '_compute_count_books')
	# function to compute books count:
	def _compute_count_books(self):
		for author in self:
			author.count_books = len(author.authored_books_ids)

# issues with the book - chapter 15 part 7
class LibraryBookIssues(models.Model):
	_name = 'book.issue'
	_description = 'Problems people reported with a book'

	book_id = fields.Many2one('library.book', required = True)
	submitted_by = fields.Many2one('res.partner') # book has res.users
	issue_description = fields.Text()



# we can redefine the function that gets the name (by default it is the 'name' record, or the record specified with _rec_name)
# can not see this working in the UI...
# seems this should be in the class instead: 
# https://github.com/PacktPublishing/Odoo-12-Development-Cookbook-Third-Edition/blob/master/Chapter05/r7_constraints/my_library/models/library_book.py
def name_get(self):
	result = []
	for record in self:
		rec_name = "%s (%s)" % (record.name, record.date_released)
		result.append((record.id, rec_name))
	return result
