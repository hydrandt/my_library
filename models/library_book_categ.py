from odoo import models, fields, api
# and for the validation:
from odoo.exceptions import ValidationError

# TODO: this is not visible in the interface yet
# Actually, I added the field, and through that field it is possible to create categories and also child categories

class BookCategory(models.Model):
	_name = 'library.book.category'
	# to enable "special hierarchy support" (nested set model? https://en.wikipedia.org/wiki/Nested_set_model)
	_parent_store = True
	_parent_name = 'parent_id' # optional, if the field is already called 'parrent_id'

	name = fields.Char('Category')
	parent_id = fields.Many2one(
		'library.book.category',
		string = 'Parent Category',
		ondelete = 'restrict',
		index = True)
	child_ids = fields.One2many(
		'library.book.category', 'parent_id',
		string = 'Child Categories')
	parent_path = fields.Char(index = True) # helper field


	# to prevent loops - I don't understand the @ thing quite yet
	@api.constrains('parent_id')
	def _check_hierarchy(self):
		if not self._check_recursion():
			raise models.ValidationError('Error! You cannot create recursive categories.') 