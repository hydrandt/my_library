{ 
	'name': 'My Library',
	'summary': 'This addon helps to manage books.',
	'description': '''Looooooooooooooooooooong description!''',
	'author': 'Lukas Helebrandt',
	'website': 'https://greensteps.com.cn',
	'category': 'Uncategorized',
	'version': '13.0.0',
	'depends': ['base', 'website'],
	'data': [
		'security/groups.xml',
		'security/ir.model.access.csv',
		'views/library_book.xml',
		'views/templates.xml',
	],

}

